﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="feedback.aspx.cs" Inherits="feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="baner about-bnr">
        <div class="container">
            <div class="baner-grids">
                <div class="col-md-6 baner-top">
                    <img src="images/img16.jpg" alt="" />
                </div>
                <div class="col-md-6 baner-top">
                    <img src="images/img17.jpg" alt="" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//baner-->
    <!--about-->

    <div class="container">
        <div class="gallery-grids">
            <h3 class="title">Offers</h3>

            <table class="table">

                <tr>
                    <td>FeedBack:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFeedback" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="btnSubmit_OnClick" />
                    </td>

                </tr>
            </table>
        </div>
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyOrder : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
  
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Uname"].ToString() != "Welcome")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
       
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from [Order] where CustomerUsername='" + Session["Uname"].ToString() + "'", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdOrder.DataSource = reader;
        grdOrder.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void grdOrder_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        

    }
}
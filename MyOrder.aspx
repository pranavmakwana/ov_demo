﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="MyOrder.aspx.cs" Inherits="MyOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="baner about-bnr">
        <div class="container">
            <div class="baner-grids">
                <div class="col-md-6 baner-top">
                    <img src="images/img16.jpg" alt="" />
                </div>
                <div class="col-md-6 baner-top">
                    <img src="images/img17.jpg" alt="" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//baner-->
    <!--about-->

    <div class="container">
        <div class="gallery-grids">
            <h3 class="title">My Orders</h3>

            <asp:GridView ID="grdOrder" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdOrder_OnRowCommand" BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
                <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("OrderId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OrderId" HeaderText="OrderId" />

                    <asp:BoundField DataField="ShippingId" HeaderText="ShippingId" />
                    <asp:BoundField DataField="BillId" HeaderText="BillId" />
                    <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" />
                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" />
                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                    <asp:BoundField DataField="OrderStatusDes" HeaderText="OrderStatusDes" />
                    <asp:BoundField DataField="EstimatedDate" HeaderText="EstimatedDate" />


                  <%--  <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>


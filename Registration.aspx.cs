﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Registration : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
    }
    protected void submitReg(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [customer] ([FirstName],[LastName],[UserName],[Email],[Phonenumber],[Gender],[Birthdate],[AddressLine1],[AddressLine2],[City],[State],[Password]) VALUES(' " + txtFName.Text + " ' , ' " + txtLName.Text + " ' , ' " + txtUName.Text + " ' , '" + txtEmail.Text + "' , ' " + txtPNo.Text + " ', '" + drpGender.SelectedValue.ToString() + " ', ' " + txtBday.Text + " ' , ' " + txtAdd1.Text + " ' ,' " + txtAdd2.Text + " ' ,' " + txtCity.Text + " ' , ' " + txtState.Text + " ' , ' " + txtPass.Text + " ')";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            loaddata();
            txtFName.Text = " ";
            txtLName.Text = " ";
            txtUName.Text = " ";
            txtEmail.Text = " ";
            txtPNo.Text = " ";
            txtBday.Text = " ";
            txtAdd1.Text = " ";
            txtAdd2.Text = " ";
            txtCity.Text = " ";
            txtState.Text = " ";
            txtPass.Text = " ";
        }
        catch (SqlException ex)
        {
            if (ex.Number == 2627)
            {
                if (ex.Message.Contains("UNIQUE"))
                {
                    lblTxt.Text = "This Email Already Exist";
                }
               

            }
        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from customer", connection);
        SqlDataReader reader = command.ExecuteReader();
        command.Dispose();
        connection.Close();
    }

    protected void resetReg(object sender, EventArgs e)
    {
        txtFName.Text = " ";
        txtLName.Text = " ";
        txtUName.Text = " ";
        txtEmail.Text = " ";
        txtPNo.Text = " ";
        txtBday.Text = " ";
        txtAdd1.Text = " ";
        txtAdd2.Text = " ";
        txtCity.Text = " ";
        txtState.Text = " ";
        txtPass.Text = " ";
    }

}
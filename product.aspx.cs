﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class product : System.Web.UI.Page
{
    ConnectionStringSettings _test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.QueryString["id"] == "")
        //{
        //    Response.Redirect("Default.aspx");
        //}
        
        //else
        //{
            DisplayVeg();
       // } 
    }
    private void DisplayVeg()
    {
        //var sb = new StringBuilder();
        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Product where CategoryId = " + Request.QueryString["id"] + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            veg.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    veg.InnerHtml += "<div class='col-sm-6 col-md-3 team-grids'> <div class='thumbnail team-thmnl'> <img src='" + dr[8].ToString() + "' class='img-responsive zoom-img' alt='...'> <div class='caption'> <h4><a href='#'>" + dr[2].ToString() + "</a></h4> <p>" + dr[4].ToString() + dr[5].ToString() + "</p> <div class='col-md-6'> <a type='button' class='btn btn-success' href='Addtocart.aspx?id=" + dr[0].ToString() + "'>Add To cart</a> </div> </div> </div></div>";
                }
            }

        }
    }
}

CREATE TABLE [dbo].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Message] [nvarchar](50) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AdminLogin]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminLogin](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_AdminLogin] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[category]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Catagory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Phonenumber] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[Birthdate] [nvarchar](50) NULL,
	[AddressLine1] [nvarchar](50) NULL,
	[AddressLine2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerLogin]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerLogin](
	[CustomerLoginId] [int] IDENTITY(1,1) NOT NULL,
	[CutomerId] [int] NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_CustomerLogin] PRIMARY KEY CLUSTERED 
(
	[CustomerLoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FeedBack]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedBack](
	[FeedBackId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[Feedback] [nvarchar](50) NULL,
 CONSTRAINT [PK_FeedBack] PRIMARY KEY CLUSTERED 
(
	[FeedBackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Offer]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offer](
	[OfferId] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED 
(
	[OfferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUsername] [nvarchar](50) NULL,
	[ShippingId] [int] NULL,
	[BillId] [nvarchar](50) NULL,
	[OrderDate] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[Quantity] [nvarchar](50) NULL,
	[UnitPrice] [nvarchar](50) NULL,
	[Total] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[OrderStatus] [int] NULL,
	[OrderStatusDes] [nvarchar](50) NULL,
	[PaymentMode] [int] NULL,
	[DeliveryBoy] [nvarchar](50) NULL,
	[EstimatedDate] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusDescription] [nvarchar](50) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentMode]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMode](
	[PaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMode] [nvarchar](50) NULL,
 CONSTRAINT [PK_PaymentMode] PRIMARY KEY CLUSTERED 
(
	[PaymentModeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Desciption] [nvarchar](500) NULL,
	[Unit_Price] [nvarchar](50) NULL,
	[Unit] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[QuantityAvailble] [nvarchar](50) NULL,
	[Image] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[DispalyonTopPage] [nvarchar](50) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Status]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TopPageImages]    Script Date: 4/22/2016 9:15:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TopPageImages](
	[Image1] [nvarchar](50) NULL,
	[Desc1] [nvarchar](50) NULL,
	[Image2] [nvarchar](50) NULL,
	[Desc2] [nvarchar](50) NULL,
	[Image3] [nvarchar](50) NULL,
	[Desc3] [nvarchar](50) NULL,
	[Image4] [nvarchar](50) NULL,
	[Desc4] [nvarchar](50) NULL,
	[Image5] [nvarchar](50) NULL,
	[Desc5] [nvarchar](50) NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AdminLogin] ON 

GO
INSERT [dbo].[AdminLogin] ([UserId], [Username], [Password]) VALUES (1, N'admin', N'admin123')
GO
SET IDENTITY_INSERT [dbo].[AdminLogin] OFF
GO
SET IDENTITY_INSERT [dbo].[category] ON 

GO
INSERT [dbo].[category] ([CategoryId], [CategoryName], [Description]) VALUES (1, N'vegetables', N'vegetables')
GO
INSERT [dbo].[category] ([CategoryId], [CategoryName], [Description]) VALUES (2, N'fruit', N'fruit')
GO
INSERT [dbo].[category] ([CategoryId], [CategoryName], [Description]) VALUES (3, N'cut vegetables', N'cut vegetables')
GO
INSERT [dbo].[category] ([CategoryId], [CategoryName], [Description]) VALUES (4, N'  Vegetables ', N'  vegetables ')
GO
SET IDENTITY_INSERT [dbo].[category] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

GO
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [LastName], [UserName], [Email], [Phonenumber], [Gender], [Birthdate], [AddressLine1], [AddressLine2], [City], [State], [Password]) VALUES (1, N'fiza', N'shaikh', N'fiza', N'fiza123@gmail.com', N'9876543210', N'female', N'7/6/1997', N'jivraj', N'jivraj', N'ahmedabad', N'gujarat', N'fiza123')
GO
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [LastName], [UserName], [Email], [Phonenumber], [Gender], [Birthdate], [AddressLine1], [AddressLine2], [City], [State], [Password]) VALUES (2, N'drashti', N'shah', N'drashti', N'drashti123@gmail.com', N'9876543210', N'female', N'31/5/1997', N'vasna', N'vasna', N'ahmedabad', N'gujarat', N'drashti123')
GO
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [LastName], [UserName], [Email], [Phonenumber], [Gender], [Birthdate], [AddressLine1], [AddressLine2], [City], [State], [Password]) VALUES (3, N'roshni', N'rathod', N'roshni', N'roshni123@gmail.com', N'9876543210', N'female', N'24/12/1997', N'panjrapol', N'panjrapol', N'ahmedabad', N'gujarat', N'roshni123')
GO
INSERT [dbo].[Customer] ([CustomerId], [FirstName], [LastName], [UserName], [Email], [Phonenumber], [Gender], [Birthdate], [AddressLine1], [AddressLine2], [City], [State], [Password]) VALUES (4, N'pooja', N'sharma', N'pooja', N'pooja123@gmail.com', N'9876543210', N'female', N'14/3/1998', N'panjrapol', N'panjrapol', N'ahmedabad', N'gujarat', N'pooja123')
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

GO
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeName], [PhoneNumber], [Email]) VALUES (1, N'bhoomi', N' 7878787878 ', N' bhoomi@bhoomi.com ')
GO
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeName], [PhoneNumber], [Email]) VALUES (2, N'Nikul', N'123456789', N'nikul@123.com')
GO
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeName], [PhoneNumber], [Email]) VALUES (3, N'Bhavin', N'7894561239', N'bhavin@123.com')
GO
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[FeedBack] ON 

GO
INSERT [dbo].[FeedBack] ([FeedBackId], [CustomerId], [Feedback]) VALUES (1, 1, N'good')
GO
INSERT [dbo].[FeedBack] ([FeedBackId], [CustomerId], [Feedback]) VALUES (2, 2, N'awesome')
GO
SET IDENTITY_INSERT [dbo].[FeedBack] OFF
GO
SET IDENTITY_INSERT [dbo].[Offer] ON 

GO
INSERT [dbo].[Offer] ([OfferId], [Image], [Description]) VALUES (1, N' img4.jpg ', N'On Potato')
GO
INSERT [dbo].[Offer] ([OfferId], [Image], [Description]) VALUES (2, N' img3.jpg ', N' 20% Off on All products ')
GO
INSERT [dbo].[Offer] ([OfferId], [Image], [Description]) VALUES (5, N' img5.jpg ', N' On All Fruits ')
GO
SET IDENTITY_INSERT [dbo].[Offer] OFF
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

GO
INSERT [dbo].[Order] ([OrderId], [CustomerUsername], [ShippingId], [BillId], [OrderDate], [ProductName], [Quantity], [UnitPrice], [Total], [Discount], [OrderStatus], [OrderStatusDes], [PaymentMode], [DeliveryBoy], [EstimatedDate]) VALUES (1, N'fiza', 1, N'20160421083110222', N'Apr 21 2016  8:31AM', N'des', N'4', N'10', N'40', N'', 3, N' available', 0, N' cx ', N'10/10/2077')
GO
INSERT [dbo].[Order] ([OrderId], [CustomerUsername], [ShippingId], [BillId], [OrderDate], [ProductName], [Quantity], [UnitPrice], [Total], [Discount], [OrderStatus], [OrderStatusDes], [PaymentMode], [DeliveryBoy], [EstimatedDate]) VALUES (2, N'drashti', 1, N'20160422081103663', N'Apr 22 2016  8:11AM', N'xx', N'6', N'10', N'60', N'', 0, N'Order Placed', 0, N'', N'')
GO
INSERT [dbo].[Order] ([OrderId], [CustomerUsername], [ShippingId], [BillId], [OrderDate], [ProductName], [Quantity], [UnitPrice], [Total], [Discount], [OrderStatus], [OrderStatusDes], [PaymentMode], [DeliveryBoy], [EstimatedDate]) VALUES (3, N'drashti', 1, N'20160422082757613', N'Apr 22 2016  8:27AM', N' tomato ', N'1', N' 30 ', N'30', N'', 0, N'Order Placed', 0, N'', N'')
GO
INSERT [dbo].[Order] ([OrderId], [CustomerUsername], [ShippingId], [BillId], [OrderDate], [ProductName], [Quantity], [UnitPrice], [Total], [Discount], [OrderStatus], [OrderStatusDes], [PaymentMode], [DeliveryBoy], [EstimatedDate]) VALUES (4, N'drashti', 1, N'20160422090822225', N'Apr 22 2016  9:08AM', N'Orange', N'2', N'80', N'160', N'', 0, N'Order Placed', 0, N'', N'')
GO
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

GO
INSERT [dbo].[OrderStatus] ([StatusId], [StatusDescription]) VALUES (3, N'Order placed')
GO
INSERT [dbo].[OrderStatus] ([StatusId], [StatusDescription]) VALUES (4, N'Order ready for shhipping')
GO
INSERT [dbo].[OrderStatus] ([StatusId], [StatusDescription]) VALUES (5, N'In Dilervery')
GO
INSERT [dbo].[OrderStatus] ([StatusId], [StatusDescription]) VALUES (6, N'Dileverd')
GO
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentMode] ON 

GO
INSERT [dbo].[PaymentMode] ([PaymentModeId], [PaymentMode]) VALUES (2, N'cod')
GO
SET IDENTITY_INSERT [dbo].[PaymentMode] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (1, 1, N'Tomato', N'Tomato', N'10', N'Rs', N'1', N'10kg', N'img\Tomato.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (2, 1, N'Potato', N'Potato', N' 30 ', N'Rs', N' 1 ', N'20kg', N'img\Potato.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (3, 1, N'Capsicum', N'Capsicum', N'50', N'Rs', N'2', N'5kg', N'img\Capsicum.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (4, 1, N'Corn', N'Corn', N'70', N'Rs', N'5', N'15kg', N'img\Corn.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (5, 2, N'Cherry', N'Cherry', N'50', N'Rs', NULL, N'2kg', N'img\Cherry.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (6, 2, N'Orange', N'Orange', N'80', N'Rs', NULL, N'1kg', N'img\Orange.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (7, 2, N'Pineaaple', N'Pineaaple', N'60', N'Rs', N'2', N'2kg', N'img\Pineapple.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (8, 2, N'Grapes', N'Grapes', N'100', N'Rs', NULL, N'25Kg', N'img\Grapes.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (9, 3, N'Peas', N'Peas', N'50', N'Rs', NULL, N'51Kg', N'img\Peas.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (10, 3, N'CauliFlower', N'CauliFlower', N'55', N'Rs', NULL, N'5Kg', N'img\CauliFlower.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (11, 3, N'Carrot', N'Carrot', N'30', N'Rs', NULL, N'2Kg', N'img\Carrot.jpg', 1, N'1')
GO
INSERT [dbo].[Product] ([ProductId], [CategoryId], [Name], [Desciption], [Unit_Price], [Unit], [Discount], [QuantityAvailble], [Image], [Status], [DispalyonTopPage]) VALUES (12, 3, N'Cabbage', N'Cabbage', N'45', N'Rs', NULL, N'1Kg', N'img\Cabbage.jpg', 1, N'1')
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

GO
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (1, N'Avaliable')
GO
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (2, N'Not Avaliable')
GO
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
INSERT [dbo].[TopPageImages] ([Image1], [Desc1], [Image2], [Desc2], [Image3], [Desc3], [Image4], [Desc4], [Image5], [Desc5]) VALUES (N'images/img1.jpg', N'Capsikum', N'images/img2.jpg', N'Tomato', N'images/img3.jpg', N'Potato', N'images/img4.jpg', N'Spinach', N'images/img5.jpg', N'Carrot')
GO
ALTER TABLE [dbo].[CustomerLogin]  WITH CHECK ADD  CONSTRAINT [FK_CustomerLogin_Customer] FOREIGN KEY([CutomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerLogin] CHECK CONSTRAINT [FK_CustomerLogin_Customer]
GO
ALTER TABLE [dbo].[FeedBack]  WITH CHECK ADD  CONSTRAINT [FK_FeedBack_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[FeedBack] CHECK CONSTRAINT [FK_FeedBack_Customer]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Catagory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[category] ([CategoryId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Catagory]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[Status] ([StatusId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Status]
GO

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserMasterPage : System.Web.UI.MasterPage
{

    ConnectionStringSettings _test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        FillPi();

        if (Session["Uname"].ToString() == "Welcome")
        {
            xx.InnerHtml = "<a href='Login.aspx'>Login</a>";
        }
        else
        {
            userid.Value = Session["Uname"].ToString();
            xx.InnerHtml = "<a href='#' class='dropdown-toggle hvr-bounce-to-bottom' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Welcome " + Session["Uname"].ToString() + "</a><ul class='dropdown-menu'><li><a class='hvr-bounce-to-bottom' href='MyOrder.aspx'>My Order</a></li><li><a class='hvr-bounce-to-bottom' href='feedback.aspx'>Feed Back</a></li> <li><a class='hvr-bounce-to-bottom' href='logout.aspx'>Log Out</a></li> </ul>";
        }
    }

    private void FillPi()
    {
        //var sb = new StringBuilder();
        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Category", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            product.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    product.InnerHtml += "<a class='hvr-bounce-to-bottom' href='product.aspx?id="+dr[0].ToString()+"'>" + dr[1].ToString() + "</a>";

                }
            }

        }
    }
}

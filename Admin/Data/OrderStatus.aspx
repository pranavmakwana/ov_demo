﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="OrderStatus.aspx.cs" Inherits="Admin_Data_OrderStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center; color: #C90000""> OrderStatus </h1>
    <table class="table">
        <tr>
            <td>StatusDescription:
            </td>
            <td>
                <asp:TextBox ID="txtStsDes" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                   <td>
                  <asp:RequiredFieldValidator ID="reqStsD" runat="server" ErrorMessage="Enter Status Descrpition" ControlToValidate="txtStsDes" ForeColor="Red"></asp:RequiredFieldValidator>

                   </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitordstatus" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="false" OnClick="resetordstatus" />
            </td>
            <td>

            </td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />
    <asp:GridView ID="grdOrdStatus" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdOrdStatus_RowCommand"  BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("StatusId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="StatusId" HeaderText="StatusId" />
            <asp:BoundField DataField="StatusDescription" HeaderText="StatusDescription" />
             


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


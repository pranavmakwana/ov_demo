﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Updateproduct.aspx.cs" Inherits="Admin_Data_Updateproduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="table">
        <tr>
            <td>UnitPrice:
            </td>
            <td>
                <asp:TextBox ID="txtuprice" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqUprice" runat="server" ErrorMessage="Enter Unit Price" ControlToValidate="txtuprice" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitUpdatePro" />
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
    
    <br/>
    <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
</asp:Content>


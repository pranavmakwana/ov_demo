﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_ToppageImages : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    protected void submitImage(object sender, EventArgs e)
    {
        if (flupImage1.HasFile && flupImage2.HasFile && flupImage3.HasFile && flupImage4.HasFile && flupImage5.HasFile)
        {
            if (flupImage1.PostedFile.ContentType == "image/jpeg" && flupImage2.PostedFile.ContentType == "image/jpeg" && flupImage3.PostedFile.ContentType == "image/jpeg" && flupImage4.PostedFile.ContentType == "image/jpeg" && flupImage5.PostedFile.ContentType == "image/jpeg")
            {
                string filename1 = Path.GetFileName(flupImage1.FileName);
                flupImage1.SaveAs(Server.MapPath("~/") + filename1);

                string filename2 = Path.GetFileName(flupImage2.FileName);
                flupImage2.SaveAs(Server.MapPath("~/") + filename2);

                string filename3 = Path.GetFileName(flupImage3.FileName);
                flupImage3.SaveAs(Server.MapPath("~/") + filename3);

                string filename4 = Path.GetFileName(flupImage4.FileName);
                flupImage4.SaveAs(Server.MapPath("~/") + filename4);

                string filename5 = Path.GetFileName(flupImage5.FileName);
                flupImage5.SaveAs(Server.MapPath("~/") + filename5);

                SqlConnection connection = new SqlConnection(test.ConnectionString);
                connection.Open();
                string sql = "INSERT INTO [TopPageImages] ([Image1],[Desc1],[Image2],[Desc2],[Image3],[Desc3],[Image4],[Desc4],[Image5],[Desc5]) VALUES(' " + filename1 + " ' , ' " + txtDesImg1.Text + " ' , ' " + filename2 + " ' , ' " + txtDesImg2.Text + " ' , ' " + filename3 + " ' , ' " + txtDesImg3.Text + " ' , ' " + filename4 + " ' , ' " + txtDesImg4.Text + " ' , ' " + filename5 + " ' , ' " + txtDesImg5.Text + " ')";
                SqlCommand command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
                connection.Close();
                lblTxt.Text = "Data Done";
            }
            else
                lblTxt.Text = "Upload status: Only JPEG files are accepted!";
        }

    }
  
}
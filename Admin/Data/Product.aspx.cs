﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_Product : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
                filldrp();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
    public void filldrp()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand("select * from Category", connection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataSet ds = new DataSet();
        da.Fill(ds);

        drpCatId.DataTextField = ds.Tables[0].Columns["CategoryName"].ToString();
        drpCatId.DataValueField = ds.Tables[0].Columns["CategoryId"].ToString();

        drpCatId.DataSource = ds.Tables[0];
        drpCatId.DataBind();
        connection.Close();

        SqlConnection con = new SqlConnection(test.ConnectionString);
        con.Open();
        SqlCommand cmd1 = new SqlCommand("select * from Status", con);
        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);

        DataSet ds1 = new DataSet();
        da1.Fill(ds1);

        drpStatus.DataTextField = ds1.Tables[0].Columns["StatusName"].ToString();
        drpStatus.DataValueField = ds1.Tables[0].Columns["StatusId"].ToString();

        drpStatus.DataSource = ds1.Tables[0];
        drpStatus.DataBind();
        con.Close();

    }
    protected void submitproduct(object sender, EventArgs e)
    {
        try
        {
            if (flupImage.HasFile)
            {
                if (flupImage.PostedFile.ContentType == "image/jpeg")
                {
                    var filename = Path.GetFileName(flupImage.FileName);
                    flupImage.SaveAs(Server.MapPath("~/") + filename);
                    SqlConnection connection = new SqlConnection(test.ConnectionString);
                    connection.Open();
                    string sql = "INSERT INTO [Product] ([CategoryId],[Name],[Desciption],[Unit_Price],[Unit],[Discount],[QuantityAvailble],[Image],[Status],[DispalyonTopPage]) VALUES(' " + drpCatId.SelectedValue.ToString() + " ' , ' " + txtName.Text + " '  , ' " + txtDes.Text + " '  , ' " + txtuprice.Text + " '  , ' " + txtUnit.Text + " ' , ' " + txtDis.Text + " ' , ' " + txtQtyAva.Text + " '  , ' " + filename + " ' , ' " + drpStatus.SelectedValue.ToString() + " ' , ' " + txtDisplay.Text + " ' )";
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    connection.Close();

                    lblTxt.Text = "Data  Done";
                    loaddata();
                    txtName.Text = " ";
                    txtUnit.Text = " ";
                    txtDes.Text = " ";
                    txtuprice.Text = " ";
                    txtDis.Text = " ";
                    txtQtyAva.Text = " ";
                    txtDisplay.Text = " ";
              }
                else
                {
                   lblTxt.Text = "Upload status: Only JPEG files are accepted!";
                }
            }

            

        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from Product", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdProduct.DataSource = reader;
        grdProduct.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void resetproduct(object sender, EventArgs e)
    {
        txtName.Text = " ";
        txtUnit.Text = " ";
        txtDes.Text = " ";
        txtuprice.Text = " ";
        txtDis.Text = " ";
        txtQtyAva.Text = " ";
        txtDisplay.Text = " ";
    }
    protected void grdProduct_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete Product WHERE ProductId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
}
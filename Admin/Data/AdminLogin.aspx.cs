﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_AdminLogin : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)

    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
    protected void submitadminlogin(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [AdminLogin] ([UserName],[Password]) VALUES(' " + txtUName.Text + " ' , ' " + txtPass.Text + " ' )";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            loaddata();
            txtUName.Text = " ";
            txtPass.Text = " ";

        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from AdminLogin", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdAdminLogin.DataSource = reader;
        grdAdminLogin.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void resetadminlogin(object sender, EventArgs e)
    {
        txtUName.Text = " ";
        txtPass.Text = " ";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_customer : System.Web.UI.Page
{
   ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
 
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from customer", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdCustomer.DataSource = reader;
        grdCustomer.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete Customer WHERE CustomerId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
    protected void submitAdd(object sender, EventArgs e)
    {
        Response.Redirect("CustomerAdd.aspx");
    }
}
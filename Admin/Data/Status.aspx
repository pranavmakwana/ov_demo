﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Status.aspx.cs" Inherits="Admin_Data_Status" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="text-align: center;color: #C90000"> Status </h1>
    <table class="table">
        <tr>
            <td>StatusName:
            </td>
            <td>
                <asp:TextBox ID="txtStatusName" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
            <td>
                 <asp:RequiredFieldValidator ID="reqstatusN" runat="server" ErrorMessage="Enter Status Name" ControlToValidate="txtStatusName" ForeColor="Red"></asp:RequiredFieldValidator>
       
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitstatus" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="false" OnClick="resetstatus" />
            </td>
            <td>
                
            </td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />



    <asp:GridView ID="grdStatus" Width="100%" runat="server" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" OnRowCommand="grdStatus_RowCommand" BorderStyle="None" BorderWidth="1px" GridLines="Horizontal" ForeColor="Black" CellPadding="4">
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />

        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("StatusId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="StatusId" HeaderText="StatusId" />
            <asp:BoundField DataField="StatusName" HeaderText="StatusName" />


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>


</asp:Content>


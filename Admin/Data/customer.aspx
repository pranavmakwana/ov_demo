﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="customer.aspx.cs" Inherits="Admin_Data_customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="text-align: center; color: #C90000">Customer </h1>

     
    <asp:Button ID="btnAdd" runat="server" Text="Add Customer" CssClass="btn btn-info" OnClick="submitAdd" />
    <br />
     <br />

    <asp:GridView ID="grdCustomer" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdCustomer_RowCommand" BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="15px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("CustomerId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" />

            <asp:TemplateField HeaderText="First last name">
                <ItemTemplate>
                    <asp:Label Visible="true" runat="server" ID="lblFName" Text='<%# Eval("FirstName") %>'></asp:Label>
                    <br />
                    <asp:Label Visible="true" runat="server" ID="lblLName" Text='<%# Eval("LastName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:BoundField DataField="UserName" HeaderText="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" />
            <asp:BoundField DataField="Phonenumber" HeaderText="PhoneNumber" />
            <asp:BoundField DataField="Gender" HeaderText="Gender" />
            <asp:BoundField DataField="Birthdate" HeaderText="Birthdate" />

            <asp:TemplateField HeaderText="Address Lime 1 & 2">
                <ItemTemplate>
                    <asp:Label Visible="true" runat="server" ID="lblAddress1" Text='<%# Eval("AddressLine1") %>'></asp:Label>
                    <br />
                    <asp:Label Visible="true" runat="server" ID="lblAddress2" Text='<%# Eval("AddressLine2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="City & State">
                <ItemTemplate>
                    <asp:Label Visible="true" runat="server" ID="lblCity" Text='<%# Eval("City") %>'></asp:Label>
                    <br />
                    <asp:Label Visible="true" runat="server" ID="lblState" Text='<%# Eval("State") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="CustomerAdd.aspx.cs" Inherits="Admin_Data_CustomerAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <table class="table">
        <tr>
            <td>First Name:
            </td>
            <td>
                <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtfname" runat="server" ErrorMessage="Enter first Name" ControlToValidate="txtFName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>Last Name:
            </td>
            <td>
                <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>

                <asp:RequiredFieldValidator ID="Rvtxtlname" runat="server" ErrorMessage="Enter Last Name" ControlToValidate="txtLName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>User Name:
            </td>
            <td>
                <asp:TextBox ID="txtUName" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rvtxtUName" runat="server" ErrorMessage="Enter User Name" ControlToValidate="txtUName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Email:
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtemail" runat="server" ErrorMessage="Required for Email" ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Enter valid emailId" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Phone Number:
            </td>
            <td>
                <asp:TextBox ID="txtPNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtpho" runat="server" ErrorMessage="Enter Phone Number" ControlToValidate="txtPNo" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Enter Number" ForeColor="Red" ControlToValidate="txtPNo" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Gender:
            </td>
            <td>
                <asp:DropDownList ID="drpGender" runat="server" AutoPostBack="true" CssClass="form-control">
                    <asp:ListItem>Select</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                    <asp:ListItem>Male</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>Birthday:
            </td>
            <td>
                <asp:TextBox ID="txtBday" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtbday" runat="server" ErrorMessage="Enter Birthday" ControlToValidate="txtBday" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Address Line1:
            </td>
            <td>
                <asp:TextBox ID="txtAdd1" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtadd1" runat="server" ErrorMessage="Enter Address" ControlToValidate="txtAdd1" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Address Line2:
            </td>
            <td>
                <asp:TextBox ID="txtAdd2" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtadd2" runat="server" ErrorMessage="Enter Address" ControlToValidate="txtAdd2" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>City:
            </td>
            <td>
                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>

                <asp:RequiredFieldValidator ID="Rvtxtcity" runat="server" ErrorMessage="Enter City Name" ControlToValidate="txtCity" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>State:
            </td>
            <td>
                <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtstate" runat="server" ErrorMessage="Enter State Name" ControlToValidate="txtState" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitcustomer" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="False" OnClick="resetcustomer" />
            </td>
            <td></td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />

    
</asp:Content>


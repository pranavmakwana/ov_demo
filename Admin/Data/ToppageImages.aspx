﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="ToppageImages.aspx.cs" Inherits="Admin_Data_ToppageImages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center;color: #C90000"> TopPageImages </h1>
    <table class="table">
        <tr>
            <td>
                <asp:FileUpload ID="flupImage1" runat="server" CssClass="form-control"/>
            </td>
            <td>
                Description:<asp:TextBox ID="txtDesImg1" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="flupImage2" runat="server" CssClass="form-control" />
            </td>
            <td>
                Description:<asp:TextBox ID="txtDesImg2" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="flupImage3" runat="server" CssClass="form-control"/>
            </td>
            <td>
                Description:<asp:TextBox ID="txtDesImg3" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="flupImage4" runat="server" CssClass="form-control"/>
            </td>
            <td>
                Description:<asp:TextBox ID="txtDesImg4" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="flupImage5" runat="server" CssClass="form-control"/>
            </td>
            <td>
                Description:<asp:TextBox ID="txtDesImg5" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitImage"/>
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" />
            </td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=""></asp:Label>
    <br />
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_UpdateOrder : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            SqlCommand cmd = new SqlCommand("select * from Employee", connection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            da.Fill(ds);

            drpDelivery.DataTextField = ds.Tables[0].Columns["EmployeeName"].ToString();
            drpDelivery.DataValueField = ds.Tables[0].Columns["EmployeeId"].ToString();

            drpDelivery.DataSource = ds.Tables[0];
            drpDelivery.DataBind();

            SqlCommand cmd1 = new SqlCommand("select * from OrderStatus", connection);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);

            DataSet ds1 = new DataSet();
            da1.Fill(ds1);

            drpStatus.DataTextField = ds1.Tables[0].Columns["StatusDescription"].ToString();
            drpStatus.DataValueField = ds1.Tables[0].Columns["StatusId"].ToString();

            drpStatus.DataSource = ds1.Tables[0];
            drpStatus.DataBind();
            connection.Close();
        }
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "UPDATE [Order] SET OrderStatus ="+drpStatus.SelectedValue+ ",OrderStatusDes ='"+drpStatus.SelectedItem+"',DeliveryBoy ='"+drpDelivery.SelectedItem+"',EstimatedDate='"+TextBox2.Text+"'WHERE Orderid ="+Request.QueryString["id"]+"";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            Response.Redirect("Order.aspx");


        }
        catch (Exception ex)
        {

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_Feedback : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            //    filldrp();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
  /*  public void filldrp()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand("select * from customer", connection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataSet ds = new DataSet();
        da.Fill(ds);

        drpFeedback.DataTextField = ds.Tables[0].Columns["FirstName"].ToString();
        drpFeedback.DataValueField = ds.Tables[0].Columns["CustomerId"].ToString();

        drpFeedback.DataSource = ds.Tables[0];
        drpFeedback.DataBind();
        connection.Close();
    }
    protected void submitfeedback(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [FeedBack] ([CustomerId],[FeedBack]) VALUES(' " + drpFeedback.SelectedValue.ToString() + " ' , ' " + txtFeedback.Text + " ' )";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            loaddata();
            txtFeedback.Text = " ";
            
        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }    */
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from FeedBack", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdFeedback.DataSource = reader;
        grdFeedback.DataBind();
        command.Dispose();
        connection.Close();
    }

 /*   protected void resetfeedback(object sender, EventArgs e)
    {
        txtFeedback.Text = " ";
    }   */

    protected void grdFeedback_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete Feedback WHERE FeedbackId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="UpdateOrder.aspx.cs" Inherits="Admin_Data_UpdateOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table class="table">
        <tr>
            <td>Order Status:
            </td>
            <td>
               <asp:DropDownList ID="drpStatus" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>

            </td>
        </tr>
        
          <tr>
            <td>Delivery Boy:
            </td>
            <td>
                <asp:DropDownList ID="drpDelivery" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>Estimated Delivery:
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td></td>
        </tr>
         <tr>
            <td>
                           <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="btnSubmit_OnClick" />
    
            </td>
            <td>
                
            </td>
            <td></td>
        </tr>

    </table>

</asp:Content>


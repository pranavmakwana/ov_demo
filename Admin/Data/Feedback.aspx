﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Feedback.aspx.cs" Inherits="Admin_Data_Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center;color: #C90000"> Feedback </h1>
  <!--  <table class="table">
     <tr>
            <td>CustomerId:
            </td>
            <td>
                <asp:DropDownList ID="drpFeedback" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>FeedBack:
            </td>
            <td>
                <asp:TextBox ID="txtFeedback" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="false" />
            </td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />
    -->
   <asp:GridView ID="grdFeedback" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdFeedback_RowCommand"  BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("FeedbackId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="FeedbackId" HeaderText="FeedbackId" />
            <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" />
             <asp:BoundField DataField="Feedback" HeaderText="Feedback" />
             

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


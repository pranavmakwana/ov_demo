﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Employee.aspx.cs" Inherits="Admin_Data_Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center;color:#C90000"> Employee </h1>
    <table class="table">
        <tr>
            <td>
                EmployeeName:
            </td>
            <td>
                <asp:TextBox ID="txtEName" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rvtxtEName" runat="server" ErrorMessage="Enter Employee Name" ControlToValidate="txtEName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                PhoneNumber:
            </td>
            <td>
                <asp:TextBox ID="txtPNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="Rvtxtpho" runat="server" ErrorMessage="Enter Phone Number" ControlToValidate="txtPNo" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Enter Number" ForeColor="Red" ControlToValidate="txtPNo" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
           <td>
               Email:
           </td>
            <td>
                 <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                 <asp:RequiredFieldValidator ID="Rvtxtemail" runat="server" ErrorMessage="Required for Email" ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Enter valid emailId" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
         <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitEmployee" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="False" OnClick="resetEmployee" />
            </td>
            <td></td>
        </tr>
    </table>
     <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />

    <asp:GridView ID="grdEmployee" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdEmployee_RowCommand"  BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("EmployeeId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EmployeeId" HeaderText="EmployeeId" />
            <asp:BoundField DataField="EmployeeName" HeaderText="EmployeeName" />
             <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" />
             <asp:BoundField DataField="Email" HeaderText="Email" />


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


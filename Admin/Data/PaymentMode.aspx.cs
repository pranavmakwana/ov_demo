﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_PaymentMode : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
    protected void submitpaymode(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [PaymentMode] ([PaymentMode]) VALUES( ' " + txtPayMode.Text + " ' )";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            loaddata();
            txtPayMode.Text = " ";

        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from PaymentMode", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdPayMode.DataSource = reader;
        grdPayMode.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void resetpaymode(object sender, EventArgs e)
    {
        txtPayMode.Text = " ";
    }
    protected void grdPayMode_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete PaymentMode WHERE PaymentModeId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
}
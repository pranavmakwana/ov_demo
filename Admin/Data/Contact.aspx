﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Admin_Data_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 style="text-align: center; color: #C90000">Contact </h1>

    <asp:GridView ID="grdContact" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdContact_RowCommand"  BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("ContactId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ContactId" HeaderText="ContactId" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
             <asp:BoundField DataField="Email" HeaderText="Email" />
              <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" />
              <asp:BoundField DataField="Message" HeaderText="Message" />


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>


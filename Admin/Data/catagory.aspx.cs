﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_catagory : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
    protected void submitcat(object sender, EventArgs e)
    {
        try{
                SqlConnection connection = new SqlConnection(test.ConnectionString);
                connection.Open();
                string sql = "INSERT INTO [category] ([CategoryName],[Description]) VALUES(' " + txtCName.Text + " ' , ' " + txtDes.Text + " ')";
                SqlCommand command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
                connection.Close();
                lblTxt.Text = "Data  Done";
                loaddata();
                txtCName.Text = " ";
                txtDes.Text = " ";
            }
        catch(Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from category", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdCat.DataSource = reader;
        grdCat.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void resetcat(object sender, EventArgs e)
    {
        txtCName.Text = " ";
        txtDes.Text = " ";
    }
}

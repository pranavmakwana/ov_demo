﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Order.aspx.cs" Inherits="Admin_Data_Order" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="text-align: center; color: #C90000">Order </h1>


    <br />

    <table class="table">
        <tr>
            <td>
                Filter:   
            </td>
            <td>
                 <asp:DropDownList ID="drpFilter" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="submitFilter"/>
            </td>
            <td>
              
            </td>
        </tr>
    </table>
    <br />

    <asp:Label ID="lblFilter" runat="server" Text=""></asp:Label>

    <asp:GridView ID="grdOrder" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdOrder_RowCommand" BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("OrderId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <b>Order ID:</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdx" Text='<%# Eval("OrderId") %>'></asp:Label>
                    <br />
                    <b>ShippingId :</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdxx" Text='<%# Eval("ShippingId") %>'></asp:Label>
                    <br />
                    <b>BillId : </b>
                    <asp:Label Visible="true" runat="server" ID="Label1" Text='<%# Eval("BillId") %>'></asp:Label>
                    <br />
                    <b>OrderDate : </b>
                    <asp:Label Visible="true" runat="server" ID="Label3" Text='<%# Eval("OrderDate") %>'></asp:Label>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemTemplate>
                    <b>ProductName:</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdxsda" Text='<%# Eval("ProductName") %>'></asp:Label>
                    <br />
                    <b>Quantity :</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdxxzxc" Text='<%# Eval("Quantity") %>'></asp:Label>
                    <br />
                    <b>UnitPrice : </b>
                    <asp:Label Visible="true" runat="server" ID="Label1zxcs" Text='<%# Eval("UnitPrice") %>'></asp:Label>
                    <br />
                    <b>Total : </b>
                    <asp:Label Visible="true" runat="server" ID="Label2asdas" Text='<%# Eval("Total") %>'></asp:Label>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemTemplate>
                    <b>OrderStatusDes:</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdxzxczxc" Text='<%# Eval("OrderStatusDes") %>'></asp:Label>
                    <br />
                    <b>PaymentMode :</b>
                    <asp:Label Visible="true" runat="server" ID="lblIdxxzxcz" Text='<%# Eval("PaymentMode") %>'></asp:Label>
                    <br />
                    <b>DeliveryBoy : </b>
                    <asp:Label Visible="true" runat="server" ID="Label1zxczx" Text='<%# Eval("DeliveryBoy") %>'></asp:Label>
                    <br />
                    <b>EstimatedDate : </b>
                    <asp:Label Visible="true" runat="server" ID="Label2xzcczasd" Text='<%# Eval("EstimatedDate") %>'></asp:Label>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField>



                <ItemTemplate>

                    <asp:HyperLink runat="server" NavigateUrl='<%# Eval("OrderId", "UpdateOrder.aspx?Id={0}") %>'>Update</asp:HyperLink>
                    <br />

                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


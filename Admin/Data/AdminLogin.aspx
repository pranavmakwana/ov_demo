﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="Admin_Data_AdminLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center;color:#C90000"> AdminLogin </h1>
     <table class="table">
     <tr>
            <td>UserName:
            </td>
            <td>
                <asp:TextBox ID="txtUName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
         <td>
                <asp:RequiredFieldValidator ID="reqName" runat="server" ErrorMessage="Enter UserName" ControlToValidate="txtUName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
      
        </tr>
        <tr>
            <td>Password:
            </td>
            <td>
                <asp:TextBox ID="txtPass" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Password" ControlToValidate="txtPass" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitadminlogin" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" CssClass="btn btn-success" OnClick="resetadminlogin" />
            </td>
            <td>

            </td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />
    

    <asp:GridView ID="grdAdminLogin" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Width="100%" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
</asp:Content>


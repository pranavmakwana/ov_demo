﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="Product.aspx.cs" Inherits="Admin_Data_Product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="text-align: center; color: #C90000">Product </h1>
    <table class="table">
        <tr>
            <td>CategoryId:
            </td>
            <td>
                <asp:DropDownList ID="drpCatId" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>Name:
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>

                <asp:RequiredFieldValidator ID="reqcName" runat="server" ErrorMessage="Enter Name" ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>Description:
            </td>
            <td>
                <asp:TextBox ID="txtDes" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqDes" runat="server" ErrorMessage="Enter Description" ControlToValidate="txtDes" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>UnitPrice:
            </td>
            <td>
                <asp:TextBox ID="txtuprice" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqUprice" runat="server" ErrorMessage="Enter Unit Price" ControlToValidate="txtuprice" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Unit:
            </td>
            <td>
                <asp:TextBox ID="txtUnit" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rvtxtUnit" runat="server" ErrorMessage="Enter Unit" ControlToValidate="txtUnit" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Discount:
            </td>
            <td>
                <asp:TextBox ID="txtDis" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqDis" runat="server" ErrorMessage="Enter Discount" ControlToValidate="txtDis" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>QuantityAvaliable:
            </td>
            <td>
                <asp:TextBox ID="txtQtyAva" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqQtyA" runat="server" ErrorMessage="Enter Avaliable Quantity " ControlToValidate="txtQtyAva" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>Image:
            </td>
            <td>
                <asp:FileUpload ID="flupImage" runat="server" CssClass="form-control" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>Status:
            </td>
            <td>
                <asp:DropDownList ID="drpStatus" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
            <td>

                <asp:RequiredFieldValidator ID="reqsta" runat="server" ErrorMessage="Enter Status" ControlToValidate="drpStatus" ForeColor="Red"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>DisplayOnTopPage:
            </td>
            <td>
                <asp:TextBox ID="txtDisplay" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitproduct" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="false" OnClick="resetproduct" />
            </td>
            <td></td>
        </tr>
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=" "></asp:Label>
    <br />
    <asp:GridView ID="grdProduct" Width="100%" runat="server" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" OnRowCommand="grdProduct_RowCommand" BorderStyle="None" BorderWidth="1px" GridLines="Horizontal" ForeColor="Black">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("ProductId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId" />
            <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" />
            <asp:BoundField DataField="Name" HeaderText="Name" />

            <asp:BoundField DataField="Desciption" HeaderText="Desciption" />
            <asp:BoundField DataField="Unit_price" HeaderText="Unit_Price" />
            <asp:BoundField DataField="Unit" HeaderText="Unit" />
            <asp:BoundField DataField="Discount" HeaderText="Discount" />




            <asp:TemplateField>
                <ItemTemplate>

                    <asp:HyperLink runat="server" NavigateUrl='<%# Eval("ProductId", "UpdateProduct.aspx?Id={0}") %>'>Update</asp:HyperLink>
                    <br />

                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>


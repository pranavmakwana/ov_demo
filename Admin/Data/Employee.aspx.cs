﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_Employee : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }

    protected void submitEmployee(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [Employee] ([EmployeeName],[PhoneNumber],[Email]) VALUES(' " + txtEName.Text + " ' , ' " + txtPNo.Text + " ', ' " + txtEmail.Text + " ' )";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            loaddata();
            txtEName.Text = " ";
            txtEmail.Text = " ";
            txtPNo.Text = " ";
        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from Employee", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdEmployee.DataSource = reader;
        grdEmployee.DataBind();
        command.Dispose();
        connection.Close();
    }

    protected void resetEmployee(object sender, EventArgs e)
    {
        txtEName.Text = " ";
        txtEmail.Text = " ";
        txtPNo.Text = " ";
    }
    protected void grdEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete Employee WHERE EmployeeId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
}
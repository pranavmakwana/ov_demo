﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Data/AdminMaster.master" AutoEventWireup="true" CodeFile="category.aspx.cs" Inherits="Admin_Data_catagory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 style="text-align:center;color: #C90000"> Category </h1>
    <table class="table">
        <tr>
            <td>
                Category Name:
            </td>
            <td>
                <asp:TextBox ID="txtCName" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                 <asp:RequiredFieldValidator ID="reqCname" runat="server" ErrorMessage="Enter Status Name" ControlToValidate="txtCName" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr> 
        <tr>
            <td>
                Description:
            </td>
            <td>
                <asp:TextBox ID="txtDes" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                 <asp:RequiredFieldValidator ID="reqDes" runat="server" ErrorMessage="Enter Description" ControlToValidate="txtDes" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr> 
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitcat" />
            </td>
            <td>
                <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" CssClass="btn btn-info" OnClick="resetcat" />
            </td>
            <td>

            </td>
        </tr> 
    </table>
    <asp:Label ID="lblTxt" runat="server" Text=""></asp:Label> <br />
    <br />
    <br />
    <asp:GridView ID="grdCat" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="false" OnRowCommand="grdCategory_RowCommand"  BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#23c1cc" Font-Bold="True" ForeColor="White" Font-Size="20px" CssClass="centerFont" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />

         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label Visible="false" runat="server" ID="lblId" Text='<%# Eval("CategoryId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" />
            <asp:BoundField DataField="CategoryName" HeaderText="CategoryName" />
             <asp:BoundField DataField="Description" HeaderText="Description" />


            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Approve" ImageUrl="../Delete.png" ToolTip="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>


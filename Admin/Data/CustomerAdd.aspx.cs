﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_CustomerAdd : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }

    protected void submitcustomer(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [customer] ([FirstName],[LastName],[UserName],[Email],[Phonenumber],[Gender],[Birthdate],[AddressLine1],[AddressLine2],[City],[State]) VALUES(' " + txtFName.Text + " ' , ' " + txtLName.Text + " ' , ' " + txtUName.Text + " ' , ' " + txtEmail.Text + " ' , ' " + txtPNo.Text + " ', '" + drpGender.SelectedValue.ToString() + " ' , ' " + txtBday.Text + " ' , ' " + txtAdd1.Text + " ' ,' " + txtAdd2.Text + " ' ,' " + txtCity.Text + " ' , ' " + txtState.Text + " ')";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            lblTxt.Text = "Data  Done";
            txtFName.Text = " ";
            txtLName.Text = " ";
            txtUName.Text = " ";
            txtEmail.Text = " ";
            txtPNo.Text = " ";
            txtBday.Text = " ";
            txtAdd1.Text = " ";
            txtAdd2.Text = " ";
            txtCity.Text = " ";
            txtState.Text = " ";

            Response.Redirect("Customer.aspx");
        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }

    protected void resetcustomer(object sender, EventArgs e)
    {
        txtFName.Text = " ";
        txtLName.Text = " ";
        txtUName.Text = " ";
        txtEmail.Text = " ";
        txtPNo.Text = " ";
        txtBday.Text = " ";
        txtAdd1.Text = " ";
        txtAdd2.Text = " ";
        txtCity.Text = " ";
        txtState.Text = " ";
    }
}
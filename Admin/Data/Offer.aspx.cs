﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_Offer : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void submitOffer(object sender, EventArgs e)
    {
        if (flupImage1.HasFile)
        {
            if (flupImage1.PostedFile.ContentType == "image/jpeg" )
            {
                string filename1 = Path.GetFileName(flupImage1.FileName);
                flupImage1.SaveAs(Server.MapPath("~/") + filename1);

                SqlConnection connection = new SqlConnection(test.ConnectionString);
                connection.Open();
                string sql = "INSERT INTO [Offer] ([Image],[Description]) VALUES(' " + filename1 + " ' , ' " + txtDesImg1.Text + " '  )";
                SqlCommand command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
                connection.Close();
                lblTxt.Text = "Data Done";
            }
            else
                lblTxt.Text = "Upload status: Only JPEG files are accepted!";
        }

    }
  
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Data_Order : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Name"].ToString() != "IT")
        {
            if (!IsPostBack)
            {
                loaddata();
                filldrp();
            }
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }


    protected void submitFilter(object sender, EventArgs e)
    { 
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            string sql = " SELECT * from [order] where OrderStatusDes='" +drpFilter.SelectedItem + "'";
            connection.Open();
            SqlCommand command = new SqlCommand(sql, connection);
         

            SqlDataReader reader = command.ExecuteReader();
            grdOrder.DataSource = reader;
            grdOrder.DataBind();
            command.Dispose();
            connection.Close();
           
        }
         catch (Exception ex)
        {
              lblFilter.Text = ex.Message;
        }
    }

    public void filldrp()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand cmd = new SqlCommand("select * from OrderStatus", connection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataSet ds = new DataSet();
        da.Fill(ds);

        drpFilter.DataTextField = ds.Tables[0].Columns["StatusDescription"].ToString();
        drpFilter.DataValueField = ds.Tables[0].Columns["StatusDescription"].ToString();

        drpFilter.DataSource = ds.Tables[0];
        drpFilter.DataBind();
        connection.Close();
    }

    //protected void submitorder(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        SqlConnection connection = new SqlConnection(test.ConnectionString);
    //        connection.Open();
    //        string sql = "INSERT INTO [Order] ([CustomerId],[ShippingId],[BillId],[OrderDate],[Total],[Discount],[Status],[PaymentMode]) VALUES(' " + drpOrder.SelectedValue.ToString() + " ' , ' " + txtSId.Text + " ' , ' " + txtBId.Text + " ' , ' " + txtOrdDate.Text + " ' ,  ' " + txtTotal.Text + " ' , ' " + txtDis.Text + " ' ,' " + drpStatus.SelectedValue.ToString() + " ' ,' " + drpPayMode.SelectedValue.ToString() + " '  )";
    //        SqlCommand command = new SqlCommand(sql, connection);
    //        command.ExecuteNonQuery();
    //        connection.Close();
    //        lblTxt.Text = "Data  Done";
    //        loaddata();
    //        txtSId.Text = " ";
    //        txtBId.Text = " ";
    //        txtOrdDate.Text = " ";
    //        txtTotal.Text = " ";
    //        txtDis.Text = " ";
    //    }
    //    catch (Exception ex)
    //    {
    //        lblTxt.Text = ex.Message;
    //    }
    //}
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from [Order]", connection);
        SqlDataReader reader = command.ExecuteReader();
        grdOrder.DataSource = reader;
        grdOrder.DataBind();
        command.Dispose();
        connection.Close();
    }

  
    protected void grdOrder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {

            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lbOrderId = (Label)gvRow.FindControl("lblId");
            SqlConnection con = new SqlConnection(test.ConnectionString);
            SqlCommand myCommand = new SqlCommand("Delete Order WHERE OrderId='" + lbOrderId.Text + "'", con);
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            loaddata();

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillId
/// </summary>
public class BillId
{

    public static string UniqueID
    {
        get
        {
            DateTime date = DateTime.Now;

            string uniqueID = String.Format(
              "{0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}{6:000}",
              date.Year, date.Month, date.Day,
              date.Hour, date.Minute, date.Second, date.Millisecond
              );
            return uniqueID;
        }
    }
}

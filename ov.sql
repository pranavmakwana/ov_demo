USE [master]
GO
/****** Object:  Database [FinalOV]    Script Date: 31-Jan-19 12:38:49 AM ******/
CREATE DATABASE [FinalOV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FinalOV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\FinalOV.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FinalOV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\FinalOV_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FinalOV] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FinalOV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FinalOV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FinalOV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FinalOV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FinalOV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FinalOV] SET ARITHABORT OFF 
GO
ALTER DATABASE [FinalOV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FinalOV] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [FinalOV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FinalOV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FinalOV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FinalOV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FinalOV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FinalOV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FinalOV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FinalOV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FinalOV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FinalOV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FinalOV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FinalOV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FinalOV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FinalOV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FinalOV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FinalOV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FinalOV] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FinalOV] SET  MULTI_USER 
GO
ALTER DATABASE [FinalOV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FinalOV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FinalOV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FinalOV] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [FinalOV]
GO
/****** Object:  Table [dbo].[AdminLogin]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminLogin](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_AdminLogin] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[category]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Catagory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Message] [nvarchar](50) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Phonenumber] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[Birthdate] [nvarchar](50) NULL,
	[AddressLine1] [nvarchar](50) NULL,
	[AddressLine2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerLogin]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerLogin](
	[CustomerLoginId] [int] IDENTITY(1,1) NOT NULL,
	[CutomerId] [int] NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_CustomerLogin] PRIMARY KEY CLUSTERED 
(
	[CustomerLoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FeedBack]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedBack](
	[FeedBackId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[Feedback] [nvarchar](50) NULL,
 CONSTRAINT [PK_FeedBack] PRIMARY KEY CLUSTERED 
(
	[FeedBackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Offer]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offer](
	[OfferId] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED 
(
	[OfferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUsername] [nvarchar](50) NULL,
	[ShippingId] [int] NULL,
	[BillId] [nvarchar](50) NULL,
	[OrderDate] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[Quantity] [nvarchar](50) NULL,
	[UnitPrice] [nvarchar](50) NULL,
	[Total] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[OrderStatus] [int] NULL,
	[OrderStatusDes] [nvarchar](50) NULL,
	[PaymentMode] [int] NULL,
	[DeliveryBoy] [nvarchar](50) NULL,
	[EstimatedDate] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusDescription] [nvarchar](50) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentMode]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMode](
	[PaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMode] [nvarchar](50) NULL,
 CONSTRAINT [PK_PaymentMode] PRIMARY KEY CLUSTERED 
(
	[PaymentModeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Desciption] [nvarchar](500) NULL,
	[Unit_Price] [nvarchar](50) NULL,
	[Unit] [nvarchar](50) NULL,
	[Discount] [nvarchar](50) NULL,
	[QuantityAvailble] [nvarchar](50) NULL,
	[Image] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[DispalyonTopPage] [nvarchar](50) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Status]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TopPageImages]    Script Date: 31-Jan-19 12:38:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TopPageImages](
	[Image1] [nvarchar](50) NULL,
	[Desc1] [nvarchar](50) NULL,
	[Image2] [nvarchar](50) NULL,
	[Desc2] [nvarchar](50) NULL,
	[Image3] [nvarchar](50) NULL,
	[Desc3] [nvarchar](50) NULL,
	[Image4] [nvarchar](50) NULL,
	[Desc4] [nvarchar](50) NULL,
	[Image5] [nvarchar](50) NULL,
	[Desc5] [nvarchar](50) NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CustomerLogin]  WITH CHECK ADD  CONSTRAINT [FK_CustomerLogin_Customer] FOREIGN KEY([CutomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerLogin] CHECK CONSTRAINT [FK_CustomerLogin_Customer]
GO
ALTER TABLE [dbo].[FeedBack]  WITH CHECK ADD  CONSTRAINT [FK_FeedBack_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[FeedBack] CHECK CONSTRAINT [FK_FeedBack_Customer]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Catagory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[category] ([CategoryId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Catagory]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[Status] ([StatusId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Status]
GO
USE [master]
GO
ALTER DATABASE [FinalOV] SET  READ_WRITE 
GO

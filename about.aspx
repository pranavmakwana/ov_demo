﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="about" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--baner-->
    <div class="baner about-bnr">
        <div class="container">
            <div class="baner-grids">
                <div class="col-md-6 baner-top">
                    <img src="images/img16.jpg" alt="" />
                </div>
                <div class="col-md-6 baner-top">
                    <img src="images/img17.jpg" alt="" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//baner-->
    <!--about-->
    <div class="about">
        <div class="container">
            <h3 class="title">About Us</h3>
            <div class="about-text">
                <div class="col-md-6 about-text-left">
                    <img src="images/img11.jpg" class="img-responsive" alt="" />
                </div>
                <div class="col-md-6 about-text-right">
                    <h4>Online Veggie</h4>
                    <p>
                      Dial Up Online Veggie is the retail venture of Care & Concern Supermarkets Pvt. Ltd. Today it is a one-stop online destination for the finest fruits, vegetables & groceries. with the growing palate preference of the consumers across Delhi-NCR we bring you the very best in service & quality. With all major brands you will find everything that you are looking for & that too without burning a hole in your pocket. You can choose from a wide range of options in every category, exclusively hand picked to suit your needs.


                    </p>
                    <p>
            We guarantee ON Time delivery & you can pay Cash on Delivery for your order. 
                        We are providing Following products
                    </p>
                    <ul>
                        <li><a href="#">Fruits</a></li>
                        <li><a href="#">Vegetables</a></li>
                        <li><a href="#">Cut Vegetables</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
       <%-- <div class="about-slid">
            <div class="container">
                <div class="about-slid-info">
                    <h2>Lorem Ipsum is has a moreless</h2>
                    <p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet embarrassing hidden in the middle of text.</p>
                </div>
            </div>
        </div>--%>
    </div>
    <%--<div class="about-team">
        <div class="container">
            <h3 class="title">Our Products</h3>
            <div class="row team-row">
                <div class="col-sm-6 col-md-3 team-grids">
                    <div class="thumbnail team-thmnl">
                        <img src="images/img12.jpg" class="img-responsive zoom-img" alt="...">
                        <div class="caption">
                            <h4><a href="#">vaura tegsner</a></h4>
                            <p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 team-grids">
                    <div class="thumbnail team-thmnl">
                        <img src="images/img13.jpg" class="img-responsive zoom-img" alt="...">
                        <div class="caption">
                            <h4><a href="#">Jark Kohnson</a></h4>
                            <p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 team-grids">
                    <div class="thumbnail team-thmnl">
                        <img src="images/img14.jpg" class="img-responsive zoom-img" alt="...">
                        <div class="caption">
                            <h4><a href="#">goes mehak</a></h4>
                            <p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 team-grids">
                    <div class="thumbnail team-thmnl">
                        <img src="images/img15.jpg" class="img-responsive zoom-img" alt="...">
                        <div class="caption">
                            <h4><a href="#">Jark Kohnson</a></h4>
                            <p>Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>--%>

</asp:Content>


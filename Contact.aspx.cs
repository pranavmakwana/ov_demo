﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Contact : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
    }

    protected void submitContact(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [Contact] ([Name],[Email],[Phonenumber],[Message]) VALUES(' " + txtName.Text + " ' , ' " + txtEmail.Text + " ' , ' " + txtPNo.Text + " ' , ' " + txtMsg.Text + " ')";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
            loaddata();
            txtName.Text = " ";
            txtEmail.Text = " ";
            txtPNo.Text = " ";
            txtMsg.Text = " ";
        }
        catch (Exception ex)
        {
            lblTxt.Text = ex.Message;
        }
    }
    public void loaddata()
    {
        SqlConnection connection = new SqlConnection(test.ConnectionString);
        connection.Open();
        SqlCommand command = new SqlCommand("select * from Contact", connection);
        SqlDataReader reader = command.ExecuteReader();
        command.Dispose();
        connection.Close();
    }

    protected void resetContact(object sender, EventArgs e)
    {
        txtName.Text = " ";
        txtEmail.Text = " ";
        txtPNo.Text = " ";
       txtMsg.Text = " ";
    }
   
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feedback : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Uname"].ToString() != "Welcome")
        {
            if (!IsPostBack)
            {
               
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
       
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = new SqlConnection(test.ConnectionString);
            connection.Open();
            string sql = "INSERT INTO [FeedBack] ([CustomerId],[FeedBack]) VALUES(' " + Session["UId"] + " ' , ' " + txtFeedback.Text + " ' )";
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();

            Response.Redirect("Default.aspx");


        }
        catch (Exception ex)
        {
          
        }
    }
}
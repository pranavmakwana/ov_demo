﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    ConnectionStringSettings _test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillPi();
            DisplayVeg();
            DisplayFruit();
        }
    }

    private void FillPi()
    {
        //var sb = new StringBuilder();
        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from TopPageImages", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            Agent.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
               Agent.InnerHtml += "<div class='col-md-6 baner-top'> <figure class='effect-bubba'> <img src='" + dr[0].ToString() + "'/> <figcaption> <h4>" + dr[1].ToString() + "</h4> </figcaption> </figure> </div> <div class='col-md-6 baner-top'> <figure class='effect-bubba'> <img src='" + dr[2].ToString() + "'/> <figcaption> <h4>" + dr[3].ToString() + "</h4> </figcaption> </figure> </div> <div class='clearfix'></div> <div class='baner-row'> <div class='col-md-4 baner-bottom'> <figure class='effect-bubba'> <img src='" + dr[4].ToString() + "'/> <figcaption> <h4>" + dr[5].ToString() + "</h4> </figcaption> </figure> </div> <div class='col-md-4 baner-bottom'> <figure class='effect-bubba'> <img src='" + dr[6].ToString() + "'/> <figcaption> <h4>" + dr[7].ToString() + "</h4> </figcaption> </figure> </div> <div class='col-md-4 baner-bottom'> <figure class='effect-bubba'> <img src='" + dr[8].ToString() + "'/> <figcaption> <h4>" + dr[9].ToString() + "</h4> </figcaption> </figure> </div> <div class='clearfix'></div> </div>";

                }
            }

        }
    }


    private void DisplayVeg()
    {
        //var sb = new StringBuilder();
        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Product where CategoryId = 1 and DispalyonTopPage = 1", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            veg.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    veg.InnerHtml += "<div class='col-sm-6 col-md-3 team-grids'> <div class='thumbnail team-thmnl'> <img src='" + dr[8].ToString() + "' class='img-responsive zoom-img' alt='...'> <div class='caption'> <h4><a href='#'>" + dr[2].ToString() + "</a></h4> <p>" + dr[4].ToString() + dr[5].ToString()+"</p> <div class='col-md-6'> <a type='button' class='btn btn-success' href='Addtocart.aspx?id=" + dr[0].ToString() + "'>Add To cart</a> </div> </div> </div></div>";
                 } 
            }

        }
    }


    private void DisplayFruit()
    {
        //var sb = new StringBuilder();

        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Product where CategoryId = 2 and DispalyonTopPage = 1", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            fruit.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    fruit.InnerHtml += "<div class='col-sm-6 col-md-3 team-grids'> <div class='thumbnail team-thmnl'> <img src='" + dr[8].ToString() + "' class='img-responsive zoom-img' alt='...'> <div class='caption'> <h4><a href='#'>" + dr[2].ToString() + "</a></h4> <p>" + dr[4].ToString() + dr[5].ToString() + "</p> <div class='col-md-6'> <a type='button' class='btn btn-success' href='Addtocart.aspx?id=" + dr[0].ToString() + "'>Add To cart</a> </div> </div> </div></div>";
                }
            }

        }
    }
}
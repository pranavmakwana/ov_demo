﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="Addtocart.aspx.cs" Inherits="Addtocartaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="baner about-bnr">
        <div class="container">
            <div class="baner-grids">
                <div class="col-md-6 baner-top">
                    <img src="images/img16.jpg" alt="" />
                </div>
                <div class="col-md-6 baner-top">
                    <img src="images/img17.jpg" alt="" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//baner-->
    <!--about-->

    <div class="container">
        <div class="gallery-grids">
            <h3 class="title">Add To Cart   </h3>

            <!--main-->
            <div class="main">
                <div class="container">
                    <div class="main-grids">

                        <div class="col-md-12 main-right">
                            <div class="col-md-4 main-text">
                                <img runat="server" id="productImage" alt="" />
                            </div>
                            <div class="col-md-8 main-text">


                                <h3>
                                    <asp:Label ID="lblProduct" runat="server" Text=""></asp:Label></h3>
                                <br />
                                <br />
                                <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lbla" runat="server" Text=""></asp:Label>

                                <%--  <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">0.25 Kg</a></li>
                                <li><a href="#">0.50 Kg</a></li>
                                <li><a href="#">1.0 Kg</a></li>
                            </ul>
                        </div>--%>
                                <br />
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem Value="1">0.25 Kg</asp:ListItem>
                                    <asp:ListItem Value="2">0.50 Kg</asp:ListItem>
                                    <asp:ListItem Value="3">0.75 Kg</asp:ListItem>
                                    <asp:ListItem Value="4">1.0 Kg</asp:ListItem>
                                    <asp:ListItem Value="5">1.25 Kg</asp:ListItem>
                                    <asp:ListItem Value="6">1.50 Kg</asp:ListItem>
                                     <asp:ListItem Value="7">1.75 Kg</asp:ListItem>
                                     <asp:ListItem Value="8">2.0 Kg</asp:ListItem>
                                     <asp:ListItem Value="9">2.25 Kg</asp:ListItem>
                                     <asp:ListItem Value="10">2.50 Kg</asp:ListItem>
                                     <asp:ListItem Value="11">2.75 Kg</asp:ListItem>
                                     <asp:ListItem Value="12">3.0 Kg</asp:ListItem>
                                     <asp:ListItem Value="13">3.25 Kg</asp:ListItem>
                                     <asp:ListItem Value="14">3.50 Kg</asp:ListItem>
                                     <asp:ListItem Value="15">3.75 Kg</asp:ListItem>
                                     <asp:ListItem Value="16">4.0 Kg</asp:ListItem>
                                     <asp:ListItem Value="17">4.25 Kg</asp:ListItem>
                                     <asp:ListItem Value="18">4.50 Kg</asp:ListItem>
                                     <asp:ListItem Value="19">4.75 Kg</asp:ListItem>
                                     <asp:ListItem Value="20">5.0 Kg</asp:ListItem>
                                </asp:DropDownList>
                                <br />

                                <table>
                                    <tr>
                                        <td>
                                            <h4>Total :</h4>
                                        </td>
                                        <td>
                                            <h3>
                                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                            </h3>

                                        </td>
                                    </tr>
                                </table>


                                <br />
                                <asp:Button runat="server" ID="btnSubit" class="more btn btn-1 btn-1b" OnClick="btnSubit_OnClick" Text="Order Submit"></asp:Button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!--//main-->
        </div>
    </div>
</asp:Content>


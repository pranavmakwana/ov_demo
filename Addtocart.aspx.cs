﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Addtocartaspx : System.Web.UI.Page
{
    ConnectionStringSettings test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Uname"].ToString() == "Welcome")
        {
            string a = Request.QueryString["id"];

            Response.Redirect("Login.aspx?id=" + a + " ");



        }

        DisplayDataItem();


    }

    public void DisplayDataItem()
    {
        SqlConnection con = new SqlConnection(test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Product where ProductId='" + Request.QueryString["id"] + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            Label1.Text = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    productImage.Src = dr[8].ToString();
                    lblProduct.Text = dr[2].ToString();
                    lblPrice.Text = dr[4].ToString();
                    lbla.Text = dr[5].ToString();
                }
            }

        }
    }


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        var a = Convert.ToInt16(lblPrice.Text);


        Label1.Text = (a * Convert.ToInt16(DropDownList1.SelectedValue)).ToString();
    }

    protected void btnSubit_OnClick(object sender, EventArgs e)
    {
       


        SqlConnection con = new SqlConnection(test.ConnectionString);

        SqlCommand cmd = new SqlCommand("INSERT INTO [Order] ([CustomerUsername] ,[ShippingId] ,[BillId] ,[OrderDate] ,[ProductName] ,[Quantity] ,[UnitPrice] ,[Total] ,[Discount] ,[OrderStatus] ,[OrderStatusDes] ,[PaymentMode] ,[DeliveryBoy] ,[EstimatedDate]) VALUES (@val1 ,@val2 ,@val3 ,@val4 ,@val5 ,@val6 ,@val7 ,@val8 ,@val9 ,@val10 ,@val11 ,@val12 ,@val13 ,@val14)", con);
        cmd.Parameters.AddWithValue("@val1", Session["Uname"].ToString());
        cmd.Parameters.AddWithValue("@val2", 1);
        cmd.Parameters.AddWithValue("@val3", BillId.UniqueID);
        cmd.Parameters.AddWithValue("@val4", System.DateTime.Now);
        cmd.Parameters.AddWithValue("@val5", lblProduct.Text);
        cmd.Parameters.AddWithValue("@val6", DropDownList1.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@val7", lblPrice.Text);
        cmd.Parameters.AddWithValue("@val8", Label1.Text);
        cmd.Parameters.AddWithValue("@val9", "");
        cmd.Parameters.AddWithValue("@val10", "");
        cmd.Parameters.AddWithValue("@val11", "Order Placed");
        cmd.Parameters.AddWithValue("@val12", "");
        cmd.Parameters.AddWithValue("@val13", "");
        cmd.Parameters.AddWithValue("@val14", "");
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();

        Response.Redirect("Default.aspx");


    }
}
package com.dev.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.dev.base.Reporter.MessageType;
import com.dev.utilities.PropertyManager;
import com.dev.utilities.ScreenshotGenerator;

public class TestBase {

	public static WebDriver driver;

	static Logger logger = Logger.getLogger("devpinoyLogger");

	/**
	 * Method to return driver
	 */
	public static WebDriver getDriver() {
		if (driver != null) {
			return driver;
		} else {
			System.out.println("Hello world" +System.getProperty("browser"));
			openAUT(System.getProperty("browser"), PropertyManager.getResourceBundle().getString("AUT"));
			return driver;
		}
	}

	/**
	 * Method to open URL
	 */
	public static void openAUT(String browsername, String AUT) {

		if (browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					PropertyManager.getResourceBundle().getString("FIREFOX_DRIVER_PATH"));
			FirefoxOptions options = new FirefoxOptions();
			driver = new FirefoxDriver(options);
			driver.manage().window().maximize();
			logger.info(AUT + " has been launched into " + browsername);

		} else if (browsername.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					PropertyManager.getResourceBundle().getString("CHROME_DRIVER_PATH"));

			driver = new ChromeDriver();
			driver.manage().window().maximize();
			logger.info(AUT + " has been launched into " + browsername);

		} else if (browsername.equalsIgnoreCase("ie")) {
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			logger.info(AUT + " has been launched into " + browsername);

		}
	}

	/**
	 * Method to quit driver
	 */
	public static void performCleanUp() {
		try {
			if (new File(PropertyManager.getResourceBundle().getString("APPLICATION_LOG_FILE")).exists()) {
				FileUtils.deleteDirectory(
						new File(PropertyManager.getResourceBundle().getString("APPLICATION_LOG_FILE")));
			}
			if (new File(PropertyManager.getResourceBundle().getString("SCREENSHOT_LOCATION")).exists()) {
				FileUtils.deleteDirectory(
						new File(PropertyManager.getResourceBundle().getString("SCREENSHOT_LOCATION")));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method to pause the execution
	 */
	public static void pause(long timeoutInMiliSeconds) {
		try {
			Thread.sleep(timeoutInMiliSeconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method to wait for page to load
	 */
	public static void waitForPageLoad(long timeOutInSeconds) {
		getDriver().manage().timeouts().implicitlyWait(timeOutInSeconds, TimeUnit.SECONDS);
	}

	/**
	 * Method to wait for page to load for Ready to Page Load State
	 */
	public static void waitForPageToLoad() {
		Wait<WebDriver> wait = new WebDriverWait(getDriver(), 20);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver input) {
				return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState")
						.equals("complete");
			}
		});
	}

	/**
	 * Method to wait for page to load for jQuery Load
	 */
	public static void waitForJQuery() {
		Wait<WebDriver> wait = new WebDriverWait(getDriver(), 20);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver input) {
				return ((JavascriptExecutor) getDriver()).executeScript("return jQuery.active==0").equals(true);
			}
		});
	}

	public static boolean waitForLoad() {

		WebDriverWait wait = new WebDriverWait(getDriver(), 30);

		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				try {
					return ((JavascriptExecutor) getDriver()).executeScript("return jQuery.active").equals(true);
					// return ((Long)executeJavaScript("return jQuery.active")
					// == 0);
				} catch (Exception e) {
					return true;
				}
			}
		};

		// wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState")
						.equals("complete");
				// return executeJavaScript("return document.readyState")
				// .toString().equals("complete");
			}
		};

		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}

	/**
	 * Method to wait until element is present
	 */
	public static void waitForElementVisible(WebElement element) {
		// need to change
		long timeOutInSeconds = Long.parseLong(PropertyManager.getResourceBundle().getString("timeout"));
		WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * Method to wait until element is present
	 * 
	 * @throws IllegalAccessException
	 */
	public static void waitForElementPresent(WebElement element) throws IllegalAccessException {
		Object proxyOrigin = FieldUtils.readField(element, "h", true);
		Object locator = FieldUtils.readField(proxyOrigin, "locator", true);
		Object findBy = FieldUtils.readField(locator, "by", true);

		String[] findByArray = findBy.toString().split(": ");
		String xPath = findByArray[1];
		// System.out.println("Xpath of element present : "+xPath);
		long timeOutInSeconds = Long.parseLong(PropertyManager.getResourceBundle().getString("timeout"));

		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
		} catch (Exception e) {
			Reporter.log("The searched element still has the class after " + timeOutInSeconds + " seconds!<br/>");
		}

	}

	/**
	 * Method to wait until element does not exist
	 * 
	 * @throws InterruptedException
	 */
	public static void waitUntilElementHasClass(WebElement element, String className, int miliseconds)
			throws InterruptedException {
		int noOfRetries = 20;
		boolean elementDisappeared = false;
		for (int i = 0; i < noOfRetries; i++) {
			// System.out.println("class : "+element.getAttribute("class"));
			if (!element.getAttribute("class").contains(className)) {
				// System.out.println("class in : "+element.getAttribute("class"));
				elementDisappeared = true;
				break;
			} else {
				// System.out.println("Sleep");
				Thread.sleep(miliseconds);
			}
		}

		if (elementDisappeared == false) {
			ScreenshotGenerator.takeScreenShot();
			Reporter.log("The searched element still has the class after " + noOfRetries * (miliseconds / 1000)
					+ " seconds!<br/>", elementDisappeared);
		}
	}

	/**
	 * Method to wait until element is clickable
	 */
	public static void waitForElementClickable(WebElement element) {
		// need to change
		long timeOutInSeconds = Long.parseLong(PropertyManager.getResourceBundle().getString("timeout"));
		WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * Method to wait until element does not exist
	 */
	public static void waitUntilElementNotVisible(WebElement element) {
		// getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		int noOfRetries = 10;
		boolean elementDisappeared = false;
		for (int i = 0; i < noOfRetries; i++) {
			try {

				element.isDisplayed();
			} catch (Exception e) {
				elementDisappeared = true;
				break;
			}
			TestBase.pause(5000);

		}
		ScreenshotGenerator.takeScreenShot();
		Reporter.log("The searched element did not disappear after " + noOfRetries * 5 + " seconds!<br/>",
				elementDisappeared);
	}

	/**
	 * Method to wait until element does not exist
	 */
	public static void waitUntilElementNotVisibleTimeout(WebElement element, int miliseconds) {
		int noOfRetries = 10;
		boolean elementDisappeared = false;
		for (int i = 0; i < noOfRetries; i++) {
			try {

				element.isDisplayed();
			} catch (Exception e) {
				elementDisappeared = true;
				break;
			}
			TestBase.pause(miliseconds);
		}

		ScreenshotGenerator.takeScreenShot();
		Reporter.log("The searched element did not disappear after time out of " + noOfRetries * (miliseconds / 1000)
				+ " seconds!<br/>", elementDisappeared);
	}

	/**
	 * Method to wait until element does not exist
	 */
	public static void waitUntilElementVisibalyHidden(WebElement element, int miliseconds) {
		int noOfRetries = 20;
		boolean elementDisappeared = false;
		for (int i = 0; i < noOfRetries; i++) {
			// System.out.println("Visibility : "+element.getCssValue("visibility"));
			if (!element.getCssValue("visibility").equals(null)) {
				if (element.getCssValue("visibility").equals("hidden")) {
					// System.out.println("Visibility in : "+element.getCssValue("visibility"));
					elementDisappeared = true;
					break;
				}
			}
			TestBase.pause(miliseconds);
		}

		if (elementDisappeared == false) {
			ScreenshotGenerator.takeScreenShot();
			Reporter.log("The searched element is still not hidden after " + noOfRetries * (miliseconds / 1000)
					+ " seconds!<br/>", elementDisappeared);
		}
	}

	/**
	 * Method to wait until element does not exist
	 */
	public static void waitUntilElementDoesntExist(WebElement element) {
		String ele_xpath = getXpath(element);
		int noOfRetries = 20;
		boolean elementDisappeared = false;
		for (int i = 0; i < noOfRetries; i++) {
			try {
				elementDisappeared = absenceOfElementLocated(By.xpath(ele_xpath));
				if (elementDisappeared) {
					break;
				}
			} catch (Exception e) {
				elementDisappeared = true;
				break;
			}
			TestBase.pause(5000);
		}

		if (!elementDisappeared) {
			Reporter.log("The searched element still exists after " + noOfRetries * 5000 + " seconds!",
					elementDisappeared);
		}
	}

	public static String getXpath(WebElement ele) {
		String str = ele.toString();
		String[] listString = null;

		if (str.contains("xpath"))
			listString = str.split("xpath:");

		return listString[1].substring(0, listString[1].length() - 1);
	}

	public static Boolean absenceOfElementLocated(final By locator) {
		try {
			driver.findElement(locator);
			return false;
		} catch (NoSuchElementException e) {
			return true;
		} catch (StaleElementReferenceException e) {
			return true;
		}
	}

	/**
	 * Method to verify true condition
	 */
	public static boolean verifyTrue(Boolean condition, String successMessage, String failureMessage) {
		if (condition) {
			Reporter.log(successMessage, MessageType.PASS);
			return true;
		} else {
			Reporter.log(failureMessage, MessageType.FAIL);
			return false;
		}
	}

	/**
	 * Method to verify false condition
	 */
	public static boolean verifyFalse(Boolean condition, String successMessage, String failureMessage) {
		if (!condition) {
			Reporter.log(successMessage, MessageType.PASS);
			return true;
		} else {
			Reporter.log(failureMessage, MessageType.FAIL);
			return false;
		}
	}

	/**
	 * Method to log message for assertation
	 */
	public static void assertTrue(boolean condition, String SuccessMessage, String FailureMessage) {
		if (condition) {
			Reporter.log(SuccessMessage, MessageType.PASS);
		} else {
			Reporter.log(FailureMessage, MessageType.FAIL);
			throw new AssertionError("Assertion Error");
		}
	}

	/**
	 * Method to log message for assertation
	 */
	public static void assertFalse(boolean condition, String SuccessMessage, String FailureMessage) {
		if (!condition) {
			Reporter.log(SuccessMessage, MessageType.PASS);
		} else {
			Reporter.log(FailureMessage, MessageType.FAIL);
			throw new AssertionError("Assertion Error");
		}
	}

	/**
	 * Method to return value of property
	 */
	public static String getProperty(String strKey) {
		try {
			InputStream input;

			input = new FileInputStream(new File("./resources/base.properties"));
			Properties property = new Properties();

			property.load(input);

			return property.getProperty(strKey);

		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	/**
	 * Method to open execution report
	 */
	public static void openTestExecutionReport() {
		openAUT(PropertyManager.getResourceBundle().getString("BROWSER1"),
				PropertyManager.getResourceBundle().getString("TEST_EXECUTION_REPORT"));
		getDriver().get(PropertyManager.getResourceBundle().getString("TEST_EXECUTION_REPORT"));
		pause(5000);
	}

	/**
	 * Method to open execution report
	 */
	public static void pageRefresh() {
		TestBase.getDriver().get(TestBase.getDriver().getCurrentUrl());
		TestBase.waitForPageToLoad();
	}

	@SuppressWarnings("deprecation")
	public static WebElement fluientWaitforElement(WebElement element, int timoutSec, int pollingSec) {

		FluentWait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
				.pollingEvery(pollingSec, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, TimeoutException.class)
				.ignoring(StaleElementReferenceException.class);

		for (int i = 0; i < 2; i++) {
			try {
				// fWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id='reportmanager-wrapper']/div[1]/div[2]/ul/li/span[3]/i[@data-original--title='We
				// are processing through trillions of data events, this insight
				// may take more than 15 minutes to complete.']")));
				fWait.until(ExpectedConditions.visibilityOf(element));
				fWait.until(ExpectedConditions.elementToBeClickable(element));
			} catch (Exception e) {

				System.out.println("Element Not found trying again - " + element.toString().substring(70));
				e.printStackTrace();

			}
		}

		return element;

	}

	public static void WaitForAjax() throws InterruptedException {
		while (true) // Handle timeout somewhere
		{

			Boolean expectation = ((JavascriptExecutor) TestBase.getDriver()).executeScript("return jQuery.active")
					.toString().equals("0");

			if (expectation)
				break;
			Thread.sleep(100);
		}
	}

	public static boolean scroll_Page(WebElement webelement, int scrollPoints) {
		try {
			Actions dragger = new Actions(TestBase.getDriver());
			// drag downwards
			int numberOfPixelsToDragTheScrollbarDown = 10;
			for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown) {
				dragger.moveToElement(webelement).clickAndHold().moveByOffset(0, numberOfPixelsToDragTheScrollbarDown)
						.release(webelement).build().perform();
			}
			Thread.sleep(500);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}

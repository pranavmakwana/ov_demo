package com.dev.base;

import org.testng.ITestResult;

import com.dev.utilities.ScreenshotGenerator;

public class Reporter extends org.testng.Reporter {

	final static String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output";

	public enum MessageType {
		INFO, PASS, FAIL;
	}
	
	/**
	 * Method to log according to message type
	 */
	public static void log(String Message, MessageType messageType) {
	
		System.setProperty(ESCAPE_PROPERTY, "false");
		
		if (messageType == MessageType.FAIL) {
			org.testng.Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);

			if (Boolean.parseBoolean(TestBase.getProperty("screenshotOnFailure")) == true) {
				
				String pathForScreenShot = ScreenshotGenerator.takeScreenShot();
				
				log("<font color='red'>FAIL </font> " + Message + "<a href=" + "img/" + pathForScreenShot
						+ ">[View ScreenShot]</a><br>");
				
			} else {
				log("<font color='red'>FAIL </font> " + Message + "<br>");
			}

		} else if (messageType == MessageType.PASS) {
			log("<font color='green'>PASS </font> " + Message + "<br>");
		} else {
			log("<font color='blue'>INFO </font> " + Message + "<br>");
		}
	}

}

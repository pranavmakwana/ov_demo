package com.dev.base;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.dev.utilities.PropertyManager;

public class WebDriverTestBase {
	
	protected static WebDriver driver;

	static PropertyManager propertyManager = new PropertyManager();
	static Logger logger = Logger.getLogger("devpinoyLogger");

	public static void openAUT(String browsername, String AUT) {
		if (browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					PropertyManager.getResourceBundle().getString("FIREFOX_DRIVER_PATH"));
			FirefoxOptions options = new FirefoxOptions();
			
			driver = new FirefoxDriver(options);
			driver.manage().window().maximize();
			driver.get(AUT);
			logger.info(AUT + " has been launched into " + browsername);
		} else if (browsername.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					PropertyManager.getResourceBundle().getString("CHROME_DRIVER_PATH"));
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(AUT);
			logger.info(AUT + " has been launched into " + browsername);
		}
	}

	public static void performCleanUp() {
		try {
			if (new File(PropertyManager.getResourceBundle().getString("APPLICATION_LOG_FILE")).exists()) {
				FileUtils.deleteDirectory(
						new File(PropertyManager.getResourceBundle().getString("APPLICATION_LOG_FILE")));
			}
			if (new File(PropertyManager.getResourceBundle().getString("SCREENSHOT_LOCATION")).exists()) {
				FileUtils.deleteDirectory(
						new File(PropertyManager.getResourceBundle().getString("SCREENSHOT_LOCATION")));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void pause(long timeoutInMiliSeconds) {
		try {
			Thread.sleep(timeoutInMiliSeconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void waitForPageLoad(long timeOutInSeconds) {
		driver.manage().timeouts().implicitlyWait(timeOutInSeconds, TimeUnit.SECONDS);
	}

	public static void waitForElementVisible(long timeOutInSeconds, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static boolean verifyTrue(Boolean condition, String successMessage, String failureMessage) {
		if (condition) {
			Reporter.log(successMessage, true);
			return true;
		} else {
			Reporter.log(failureMessage, false);
			return false;
		}
	}

	public static boolean verifyFalse(Boolean condition, String successMessage, String failureMessage) {
		if (!condition) {
			Reporter.log(successMessage, true);
			return true;
		} else {
			Reporter.log(failureMessage, false);
			return false;
		}
	}

	public static void assertTrue(boolean condition, String SuccessMessage, String FailureMessage) {
		if (condition) {
			Reporter.log(SuccessMessage, true);
		} else {
			Reporter.log(FailureMessage, false);
			throw new AssertionError("Assertion Error");
		}
	}

	public static void assertFalse(boolean condition, String SuccessMessage, String FailureMessage) {
		if (!condition) {
			Reporter.log(SuccessMessage, true);
		} else {
			Reporter.log(FailureMessage, false);
			throw new AssertionError("Assertion Error");
		}
	}

	public static void getscreenshot() {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File screenshotName = new File(PropertyManager.getResourceBundle().getString("SCREENSHOT_LOCATION")
				+ System.currentTimeMillis() + ".png");
		try {
			FileUtils.copyFile(scrFile, screenshotName);
			logger.info("Taking screenshot");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void getScreenshotOnFailure(String fileName) {
		int exceptionCounter = 0;
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(
					System.getProperty("user.dir") + "\\Screenshots\\" + fileName + "_" + exceptionCounter + ".jpg"));
			logger.info("Taking screenshot");
			exceptionCounter++;
		} catch (IOException e) {

		}
	}
	
	public static void openTestExecutionReport() {
		openAUT(PropertyManager.getResourceBundle().getString("BROWSER1"),
				PropertyManager.getResourceBundle().getString("TEST_EXECUTION_REPORT"));
	}
}

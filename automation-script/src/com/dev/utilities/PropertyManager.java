package com.dev.utilities;

import org.apache.log4j.Logger;

public class PropertyManager {
	static Logger log = Logger.getLogger(PropertyManager.class.getName());

	/**
	 * Thread Safe Properties Object
	 */

	private static ThreadLocal<Property> Properties = new ThreadLocal<Property>() {
		protected Property initialValue() {
			Property p = new Property(System.getProperty("base.property.file",
					"resources/base.properties"));
			return p;
		};
	};

	/**
	 * Resource Bundler Object
	 * 
	 */

	public static Property getResourceBundle() {
		return Properties.get();
	}

	public static void test() {
		System.out.println("Hi");
	}
}
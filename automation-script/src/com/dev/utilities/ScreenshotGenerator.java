package com.dev.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;

import com.dev.base.TestBase;

public class ScreenshotGenerator {

	/**
	 * Method to take screen shot on failure
	 */
	public static String takeScreenShot() {
		File image = null;
		
		File scrFile = ((TakesScreenshot) TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
		String fileName =Reporter.getCurrentTestResult().getName() + "_"
				+ String.valueOf(RandomNumberGenerator.generateRandom(5)) + ".png"
				;
		image = new File(PropertyManager.getResourceBundle().getString("test.results.screenshots.dir") + fileName);
		try {
			FileUtils.copyFile(scrFile, image);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//FileUtils.copyFile(scrFile, new File(image));
		return fileName;
	}
}

package com.dev.utilities;

public enum Keys {
	AUT_URL("AUT_URL", ""), DEFAULT_TIME_OUT("default_wait_timeout", "30000"), RESOURCE_DIR(
			"resources", "resources"), WEBDRIVER_SERVER_IP(
			"webdriver.server.ip", "localhost"), WEBDRIVER_SERVER_PORT(
			"webdriver.server.port", "4444"), BROWSER("browser", "firefox");

	String value;
	String defaultValue;

	private Keys(String value) {
		this.value = value;
	}

	private Keys(String value, String defaultValue) {
		this.value = value;
		this.defaultValue = defaultValue;
	}

	public String getValue() {
		return value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}
}

package com.dev.utilities;

import java.util.Random;

//Takes an array of integers and returns a random integer

public class RandomNumberGenerator {
	
	/**
	 * Method to generate random number of specified length
	 */
	public static long generateRandom(int length) {
		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return Long.parseLong(new String(digits));
	}
	
	/**
	 * Method to generate random number in US format
	 */
	public static String genearteRandomUSNumber() {
		int num1, num2, num3;
		int set2, set3;
		String strUSNumber;
		
		Random generator = new Random();

		num1 = generator.nextInt(7) + 1;
		num2 = generator.nextInt(8);
		num3 = generator.nextInt(8);

		set2 = generator.nextInt(643) + 100;
		set3 = generator.nextInt(8999) + 1000;
		
		strUSNumber="(" + num1 + "" + num2 + "" + num3 + ")" + "-" + set2 + "-" + set3;
		
		return strUSNumber;
	}
	
	
	

}

package com.dev.locators;

public interface WebLocators {

	public interface LoginPageLocators {
		
		
		String USERNAME="//input[@id='ContentPlaceHolder1_username']";
		String PASSWORD="//input[@id='ContentPlaceHolder1_password']";
		String SIGN_IN_BTN="//input[@id='ContentPlaceHolder1_Button1']";
		String OPTIONS="(//a[@class='dropdown-toggle hvr-bounce-to-bottom'])[2]";
		String LOGOUT="//a[contains(text(),'Log Out')]";
		String CONTACT_US="//a[text() = 'Contact Us']";
		String NAME="//input[@id='ContentPlaceHolder1_txtName']";
		String EMAILID="//input[@id='ContentPlaceHolder1_txtEmail']";
		String PHONENUMBER="//input[@id='ContentPlaceHolder1_txtPNo']";
		String MESSAGE="//input[@id='ContentPlaceHolder1_txtMsg']";
		String SUBMIT="//input[@value='Submit']";
		String MYORDERS="//a[contains (text(), 'My Order')]";
		String OFFERS="//a[contains (text(), 'Offers')]";
		
		
	}		
}
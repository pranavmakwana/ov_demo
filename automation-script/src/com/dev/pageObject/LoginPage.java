package com.dev.pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dev.base.TestBase;
import com.dev.locators.WebLocators.LoginPageLocators;

public class LoginPage implements LoginPageLocators {
	// Constructor
	public LoginPage() {
		PageFactory.initElements(TestBase.getDriver(), this);
	
	}
	
	
	@FindBy(xpath = CONTACT_US)
	private WebElement contactUs;
	
	@FindBy(xpath = NAME)
	private WebElement name;
	
	@FindBy(xpath = PHONENUMBER)
	private WebElement phoneNmber;
	
	@FindBy(xpath = EMAILID)
	private WebElement emailId;
	
	@FindBy(xpath = MESSAGE)
	private WebElement message;
	
	public WebElement getContactUs() {
		return contactUs;
	}

	public WebElement getName() {
		return name;
	}

	public WebElement getPhoneNmber() {
		return phoneNmber;
	}

	public WebElement getEmailId() {
		return emailId;
	}

	public WebElement getMessage() {
		return message;
	}

	public WebElement getSubmit() {
		return submit;
	}

	public WebElement getMyOrders() {
		return myOrders;
	}

	public WebElement getOffers() {
		return offers;
	}


	@FindBy(xpath = SUBMIT)
	private WebElement submit;
	
	@FindBy(xpath = MYORDERS)
	private WebElement myOrders;
	
	@FindBy(xpath = OFFERS)
	private WebElement offers;
	
	@FindBy(xpath = USERNAME)
	private WebElement username;
	
	@FindBy(xpath = PASSWORD)
	private WebElement password;
	
	@FindBy(xpath = OPTIONS)
	private WebElement options;
	
	public WebElement getOptions() {
		return options;
	}

	public WebElement getLogout() {
		return logout;
	}
	
	@FindBy(xpath = LOGOUT)
	private WebElement logout;
	
	

	public WebElement getUsername() {
		return username;
	}

	public WebElement getPassword() {
		return password;
	}

	public WebElement getSignInBtn() {
		return signInBtn;
	}
	@FindBy(xpath = SIGN_IN_BTN)
	private WebElement signInBtn;

	
	}	




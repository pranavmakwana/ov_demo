package com.dev.listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.dev.base.TestBase;
import com.dev.utilities.ScreenshotGenerator;


public class TestSuiteListener implements ITestListener {

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("***** Error " + result.getName() + " test has failed *****");
		
		org.testng.Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);

		if (Boolean.parseBoolean(TestBase.getProperty("screenshotOnFailure")) == true) {
			
			String pathForScreenShot = ScreenshotGenerator.takeScreenShot();
			
			Reporter.log("<font color='red'>FAIL </font> <a href=" + "img/" + pathForScreenShot
					+ ">[View ScreenShot]</a><br>");
			
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
//		TestBase.getDriver().quit();
	}

}

package com.dev.tests;

import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.dev.base.TestBase;
import com.dev.pageObject.LoginPage;
import com.dev.utilities.PropertyManager;

public class LoginTestCases {

	@BeforeClass
	public void beforeClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@BeforeMethod
	public void beforeMethod() throws IllegalAccessException {
	}

	@BeforeSuite
	public void beforeSuite() throws IllegalAccessException {
		LoginPage loginpage = new LoginPage();
		Reporter.log("Browser Opened");
		TestBase.getDriver().get(PropertyManager.getResourceBundle().getString("BASE_URL"));
		TestBase.verifyTrue(loginpage.getUsername().isDisplayed(), "Login Page appeard correctly.",
				"Login Page did not appear as expected.");

	}

	@Test(priority = 1, description = "Login Page")
	public void TC_01() throws IllegalAccessException {
		LoginPage loginPage = new LoginPage();
		// Log into the eFax MyAccount Application 
		
		
	
		TestBase.waitForElementVisible(loginPage.getUsername());
		loginPage.getUsername().sendKeys(PropertyManager.getResourceBundle().getString("username"));
		TestBase.waitForElementVisible(loginPage.getPassword());
		loginPage.getPassword().sendKeys(PropertyManager.getResourceBundle().getString("password"));

		// Click on "Log In" button
		loginPage.getSignInBtn().click();
		TestBase.waitForPageToLoad();
		TestBase.pause(5000);
		
		TestBase.verifyTrue(loginPage.getOptions().isDisplayed(), "User is loggedin sucessfully",
				"User is not logged in");
		
		//My Orders Page
		loginPage.getOptions().click();
		loginPage.getMyOrders().click();
		TestBase.verifyTrue(TestBase.getDriver().getCurrentUrl().contains("MyOrder"), "When user clicks on MyOrder user will redirected to MyOrder page",
				"When user clicks on MyOrder user will not be able redirected to MyOrder page");
		
		//My Offers Page
		loginPage.getOffers().click();
		TestBase.verifyTrue(TestBase.getDriver().getCurrentUrl().contains("Offers"), "When user clicks on Offers user will redirected to Offers page",
				"When user clicks on Offers user will not be able redirected to Offers page");
		
		
		//Logout Function
		loginPage.getOptions().click();
		TestBase.pause(5000);
		loginPage.getLogout().click();
	}

	@AfterSuite
	public void logout() {
	}

	@AfterSuite
	public void tearDown() {
		TestBase.getDriver().manage().deleteAllCookies();
		TestBase.getDriver().quit();
	}
}

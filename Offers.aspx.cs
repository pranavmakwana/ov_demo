﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Offers : System.Web.UI.Page
{
    ConnectionStringSettings _test = ConfigurationManager.ConnectionStrings["OnlineVegg"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillPi();
        }
    }

    private void FillPi()
    {
        //var sb = new StringBuilder();
        SqlConnection con = new SqlConnection(_test.ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter("Select * from Offer", con);
        DataSet ds = new DataSet();
        da.Fill(ds, "ov");
        if (ds.Tables[0].Rows.Count < 1)
        {
            agent.InnerHtml = "No Data";
        }
        else
        {

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    agent.InnerHtml += "<div class='col-md-4 port-grids view view-fourth'> <a class='example-image-link' href='" + dr[1].ToString() + "' data-lightbox='example-set' data-title=''> <img src=' " + dr[1].ToString() + "' class='img-responsive' alt='' /> <div class='mask'> <p>" + dr[2].ToString() + "</p> </div> </a> </div>";
                }
            }

        }
    }
}
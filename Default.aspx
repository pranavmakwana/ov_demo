﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function ShowCurrentTime(name) {
            alert(name);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--baner-->
    <div class="baner">
        <div class="container">
            <div class="baner-grids">
                <div runat="server" id="Agent"></div>

            </div>
        </div>
    </div>



    <div class="about-team">
        <div class="container">
            <h3 class="title">Top Vegetable</h3>
            <div class="row team-row">
                
                <div id="veg" runat="server"> </div>
         
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    
    
    <div class="about-team">
        <div class="container">
            <h3 class="title">Top Fruits</h3>
            <div class="row team-row">
                    <div id="fruit" runat="server"> </div>
            </div>
        </div>
    </div>
    <!-- footer -->
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.master" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <table class="table">
            <tr>
                <td>First Name:
                </td>
                <td>
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvfName" runat="server" ErrorMessage="Enter First Name" ControlToValidate="txtFName" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>

            </tr>
            <tr>
                <td>Last Name:
                </td>
                <td>
                    <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>

                    <asp:RequiredFieldValidator ID="rvlName" runat="server" ErrorMessage="Enter Last Name" ControlToValidate="txtLName" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>UserName:
                </td>
                <td>
                    <asp:TextBox ID="txtUName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvUser" runat="server" ErrorMessage="Enter User Name" ControlToValidate="txtUName" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Email:
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvEmail" runat="server" ErrorMessage="Required for Email" ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Enter valid EmailId" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Phone Number:
                </td>
                <td>
                    <asp:TextBox ID="txtPNo" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvpho" runat="server" ErrorMessage="Enter Phone Number" ControlToValidate="txtPNo" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexPhone" runat="server" ErrorMessage="Enter 10 Digits" ForeColor="Red" ControlToValidate="txtPNo" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Gender:
                </td>
                <td>
                    <asp:DropDownList ID="drpGender" runat="server" AutoPostBack="true" CssClass="form-control">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                        <asp:ListItem>Male</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Birthday:
                </td>
                <td>
                    <asp:TextBox ID="txtBday" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvbday" runat="server" ErrorMessage="Enter Birthday" ControlToValidate="txtBday" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Address Line1:
                </td>
                <td>
                    <asp:TextBox ID="txtAdd1" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvAdd1" runat="server" ErrorMessage="Enter Address" ControlToValidate="txtAdd1" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Address Line2:
                </td>
                <td>
                    <asp:TextBox ID="txtAdd2" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvAdd2" runat="server" ErrorMessage="Enter Address" ControlToValidate="txtAdd2" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>City:
                </td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>

                    <asp:RequiredFieldValidator ID="rvCity" runat="server" ErrorMessage="Enter City Name" ControlToValidate="txtCity" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>State:
                </td>
                <td>
                    <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvState" runat="server" ErrorMessage="Enter State Name" ControlToValidate="txtState" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Password:
                </td>
                <td>
                    <asp:TextBox ID="txtPass" TextMode="Password" CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvPass" runat="server" ErrorMessage="Enter Password" ControlToValidate="txtPass" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Confirm Password:
                </td>
                <td>
                    <asp:TextBox ID="txtCPass" TextMode="Password" CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rvCPass" runat="server" ErrorMessage="Enter Confirm Password" ControlToValidate="txtCPass" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmpCPass" runat="server" ErrorMessage="Enter Correct Password" ControlToCompare="txtPass" ControlToValidate="txtCPass" ForeColor="Red"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="submitReg" />
                </td>
                <td>
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-info" CausesValidation="False" OnClick="resetReg" />
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <asp:Label ID="lblTxt" runat="server" Text=""></asp:Label>
</asp:Content>

